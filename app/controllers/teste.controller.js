'use strict';

angular.module('Teste')
  .controller('TesteController', TesteController);

function TesteController ($rootScope, $scope, TesteService) {

    $scope.confirm = false;

    $scope.enviarDados = () => {
        let mensagem = {
            "name": $scope.nome,
            "email": $scope.email,
            "telefone": $scope.telefone
        }
        let dados = "from=" + "postmaster@sandboxa00e22eef59f4bc389aefbe4a247deeb.mailgun.org" + "&to=" + "juniorcastrov@gmail.com, ronalso@mgplancomunicacao.com.br" + "&subject=" + "Teste MgPlan" + "&text=" + "nome: " + $scope.nome + " email: " + $scope.email + " telefone: " + $scope.telefone
        TesteService.putDados(dados).then((data) => {
		    $scope.nome = null;		
            $scope.email = null;	
            $scope.telefone = null;	
            $scope.confirm = true;
        }).catch((err) => {

        });
    }

    $scope.bot = () => {
        $('html, body').animate({ scrollTop: $(document).height() }, 'slow');
    }

}

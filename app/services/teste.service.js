'use strict';

angular.module('Teste')
  .factory('TesteService', TesteService);

function TesteService($http, $q, httpRequest) {
  var mailgunUrl = "sandboxa00e22eef59f4bc389aefbe4a247deeb.mailgun.org";
  const factory = {
    
    putDados: (data) => {
      const url = mailgunUrl;
      const method = 'POST';
      return httpRequest(url, method, data, $q, $http);
    }
  };
  return factory;
}
